class AddressInstantSuggest {

    static instance;

    static create(options) {
        if (AddressInstantSuggest.instance != null) {
            return AddressInstantSuggest.instance;
        } else {
            AddressInstantSuggest.instance = new AddressInstantSuggest(options);
            return AddressInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantSuggest.$j;
    }

    static setJQuery(jq) {
        return AddressInstantSuggest.$j = jq;
    }

    static getUpdated(term, suggestions) {
        if (suggestions.length > 0) {
            AddressInstantSuggest.cache[term] = suggestions;
            return suggestions;
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in AddressInstantSuggest.cache) {
                return AddressInstantSuggest.cache[term];
            }

            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestAddress?etoken=" + this._options.token + "&address=" + term,
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    data.response.values.forEach(function (a) {
                        suggestions.push({label: a.suggestion});
                    });
                    AddressInstantSuggest.getUpdated(term, suggestions);
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}

class AddressInstantCorrect {

    static instance;

    static create(options) {
        if (AddressInstantCorrect.instance != null) {
            return AddressInstantCorrect.instance;
        } else {
            AddressInstantCorrect.instance = new AddressInstantCorrect(options);
            return AddressInstantCorrect.instance;
        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};

    static $j = jQuery.noConflict();

    static getJQuery() {
        return AddressInstantCorrect.$j;
    }

    static setJQuery(jq) {
        return AddressInstantCorrect.$j = jq;
    }

    correct(term) {
        if (typeof this._options !== "undefined" && this._options != null) {

            let address_line = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.address_line);
            let zip = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.zip);
            let district = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.district);
            let area = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.area);
            let city = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.city);
            let street = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.street);
            let num = AddressInstantCorrect.$j('#' + AddressInstantCorrect.instance._options.number);

            AddressInstantCorrect.$j.get({
                url: this._options.host + "/api/v1/correctAddress?etoken=" + this._options.token + "&address=" + term,
                async: false,
                timeout: 1000
            }).then(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    address_line.attr('class', AddressInstantCorrect.instance._options.verified);
                    address_line.val(data.response.postCode + ", "
                        + data.response.area + " обл., "
                        + data.response.localityType
                        + data.response.locality
                        + ", " + data.response.streetType
                        + data.response.street
                        + ", буд." + data.response.streetNumber);
                    zip.val(data.response.postCode || "");
                    district.val(data.response.area || "");
                    area.val(data.response.region || "");
                    city.val(data.response.localityType + data.response.locality || "");
                    street.val(data.response.streetType + data.response.street || "");
                    num.val(data.response.streetNumber || "");
                } else {
                    address_line.attr('class', AddressInstantCorrect.instance._options.error);
                    address_line.val("");
                    zip.val("");
                    district.val("");
                    area.val("");
                    city.val("");
                    street.val("");
                    num.val("");
                }
            }).fail(function () {
                address_line.attr('class', AddressInstantCorrect.instance._options.error);
                address_line.val("");
                zip.val("");
                district.val("");
                area.val("");
                city.val("");
                street.val("");
                num.val("");
            });
        }
    }
}

class FullnameInstantSuggest {

    static instance;

    static create(options) {
        if (FullnameInstantSuggest.instance != null) {
            return FullnameInstantSuggest.instance;
        } else {
            FullnameInstantSuggest.instance = new FullnameInstantSuggest(options);
            return FullnameInstantSuggest.instance;

        }
    }

    constructor(options) {
        this._options = options;
    }

    get options() {
        return this._options;
    }

    set options(value) {
        this._options = value;
    }

    _options = {};

    static cache = {};
    static suggestions_cache = [];
    static suggestions_plain = [];

    static $j = jQuery.noConflict();

    static getJQuery() {
        return FullnameInstantSuggest.$j;
    }

    fill(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (AddressInstantCorrect.$j.inArray(term, FullnameInstantSuggest.suggestions_plain) > -1) {
                let fiog = FullnameInstantSuggest.suggestions_cache[AddressInstantCorrect.$j.inArray(term, FullnameInstantSuggest.suggestions_plain)];
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.fullname).val(fiog.suggestion);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.name).val(fiog.name);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.thirdname).val(fiog.thirdName);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.surname).val(fiog.surName);
                AddressInstantCorrect.$j('#' + FullnameInstantSuggest.instance._options.gender).val(fiog.gender);
            }
        }
    }

    suggest(term) {
        if (typeof this._options !== "undefined" && this._options != null) {
            if (term in FullnameInstantSuggest.cache) {
                return FullnameInstantSuggest.cache[term];
            }
            let suggestions = [];
            AddressInstantCorrect.$j.ajax({
                url: this._options.host + "/api/v1/suggestFullName?etoken=" + this._options.token + "&name=" + term,//.replace(/ /g,"%20")
                async: false,
                timeout: 1000
            }).done(function (data) {
                if (typeof data.response !== "undefined" && data.response != null) {
                    FullnameInstantSuggest.suggestions_cache = [];
                    FullnameInstantSuggest.suggestions_plain = [];
                    data.response.values.forEach(function (a) {
                        suggestions.push({label: a.suggestion});
                        FullnameInstantSuggest.suggestions_cache.push(a);
                        FullnameInstantSuggest.suggestions_plain.push(a.suggestion);
                    });

                    FullnameInstantSuggest.cache[term] = suggestions;
                }
            }).fail(function () {
                console.log("Request failed for : " + term);
            });
            return suggestions;
        }
    }
}


AddressInstantSuggest.getJQuery()(document).ready(function () {
    AddressInstantSuggest.getJQuery().fn.addressLineInstantSuggest = function (options) {
        const address = AddressInstantSuggest.create(options);
        AddressInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
            minLength: 1,
            source: function (request, response) {
                let r = address.suggest(request.term);
                response(r);
            }
        });
    };
});

AddressInstantCorrect.getJQuery()(document).ready(function () {
    AddressInstantCorrect.getJQuery().fn.addressLineInstantCorrect = function (options) {

        const address = AddressInstantCorrect.create(options);

        AddressInstantCorrect.getJQuery().fn.enterKey = function (fnc) {
            return this.each(function () {
                AddressInstantCorrect.getJQuery()(this).keypress(function (ev) {
                    let keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        AddressInstantCorrect.getJQuery()('#' + options.input_id).on('autocompletechange change', function () {
            address.correct(AddressInstantCorrect.getJQuery()(this).val());
        }).change();

        AddressInstantCorrect.getJQuery()('#' + options.button_id).click(function (event) {
            event.preventDefault();
            AddressInstantCorrect.getJQuery()(".ui-menu-item").hide();
            address.correct(AddressInstantCorrect.getJQuery()('#' + id).val());
        });

        AddressInstantCorrect.getJQuery()('#' + options.input_id).enterKey(function () {
            AddressInstantCorrect.getJQuery()(".ui-menu-item").hide();
            address.correct(AddressInstantCorrect.getJQuery()(this).val());
        });

    };
});

FullnameInstantSuggest.getJQuery()(document).ready(function () {
    FullnameInstantSuggest.getJQuery().fn.fullnameLineInstantSuggest = function (options) {
        const fullname = FullnameInstantSuggest.create(options);

        FullnameInstantSuggest.getJQuery()(function () {
            FullnameInstantSuggest.getJQuery()('#' + options.input_id).autocomplete({
                minLength: 2,
                source: function (request, response) {
                    let r = fullname.suggest(request.term);
                    response(r);
                }
            });
        });

        FullnameInstantSuggest.getJQuery().fn.enterKey = function (fnc) {
            return this.each(function () {
                FullnameInstantSuggest.getJQuery()(this).keypress(function (ev) {
                    let keycode = (ev.keyCode ? ev.keyCode : ev.which);
                    if (keycode == '13') {
                        fnc.call(this, ev);
                    }
                })
            })
        };

        FullnameInstantSuggest.getJQuery()('#' + options.input_id).enterKey(function () {
            fullname.fill(this.value);
        });

        FullnameInstantSuggest.getJQuery()('#' + options.input_id).on('autocompletechange change', function () {
            fullname.fill(this.value);
        }).change();
    }
});

AddressInstantSuggest.getJQuery()(document).ready(function () {
    AddressInstantSuggest.getJQuery().fn.addressLineInstantSuggest({
        host: "https://correctme.com.ua",
        token: correctme_instant_token.token,
        input_id: "address_suggest"
    });
});
AddressInstantCorrect.getJQuery()(document).ready(function () {
    AddressInstantCorrect.getJQuery().fn.addressLineInstantCorrect({
        host: "https://correctme.com.ua",
        token: correctme_instant_token.token,
        input_id: "address_suggest",
        button_id: "address_suggest_button",
        zip: "correctZipcode",
        address_line: "correctAddress",
        district: "correctDistrict",
        area: "correctArea",
        city: "correctCity",
        street: "correctStreet",
        number: "correctNumber",
        error: "input-box-error", // switch css class in case of address error
        verified: "input-box-ok" // switch css class in case of verified
    });
});
FullnameInstantSuggest.getJQuery()(document).ready(function () {
    FullnameInstantSuggest.getJQuery().fn.fullnameLineInstantSuggest({
        host: "https://correctme.com.ua",
        token: correctme_instant_token.token,
        input_id: "fullname_suggest",
        fullname: "correctFullname",
        name: "correctName",
        thirdname: "correctThirdname",
        surname: "correctSurname",
        gender: "correctGender"
    });
});

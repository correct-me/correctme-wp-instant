<?php
/**
 * The main plugin class
 *
 * @package CMEI
 */

/**
 * Class CMEI_Plugin
 */
class CMEI_Plugin {

	/**
	 * The URL to the plugin.
	 *
	 * @var string
	 */
	private $plugin_url = '';

	/**
	 * The path to the plugin.
	 *
	 * @var string
	 */
	private $plugin_path = '';

	/**
	 * The api key.
	 *
	 * @var string
	 */
	private $api_key = '';

	/**
	 * Setup the plugin.
	 */
	public function setup() {

		$this->plugin_path = dirname( __FILE__ );
		$this->plugin_url = plugins_url( '/', $this->plugin_path );

		$options = get_option( 'cmei', array() );
		if ( ! empty( $options['api-key'] )  ) {
			$this->api_key = $options['api-key'];
		}

		if ( is_admin() ) {
			require_once( dirname( __FILE__ ) . '/Admin.php' );
			$admin = new CMEI_Admin();
			$admin->setup( $this->plugin_path, $this->plugin_url);
		}
	}
}

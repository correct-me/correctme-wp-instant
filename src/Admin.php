<?php
/**
 * Manages the admin interface.
 *
 * @package CMEI
 */

/**
 * Class CMEI_Admin
 */
class CMEI_Admin {

	/**
	 * The URL to the plugin.
	 *
	 * @var string
	 */
	private $plugin_url = '';

	/**
	 * The path to the plugin.
	 *
	 * @var string
	 */
	private $plugin_path = '';

	/**
	 * The update nonce action.
	 *
	 * @var string
	 */
	private $action = 'cmei-upate';

	/**
	 * The update nonce action name.
	 *
	 * @var string
	 */
	private $action_name = 'cmei_nonce';


	/**
	 * The options defaults
	 *
	 * @var array
	 */
	private $defaults = array();


	/**
	 * Setup the admin.
	 *
	 * @param string         $plugin_path The path to the plugin.
	 * @param string         $plugin_url The URL to the plugin.
	 */
	public function setup( $plugin_path, $plugin_url) {

		$this->plugin_path = $plugin_path;
		$this->plugin_url  = $plugin_url;

		$this->defaults = array(
			'api-key' => '',
		);

		add_action( 'admin_menu', array( $this, 'admin_menu' ) );
		add_action( 'admin_enqueue_scripts', array( $this, 'register_scripts' ) );
		if (
			current_user_can( 'manage_options' )
			&& ! empty( $_POST[ $this->action_name ] )
			&& wp_verify_nonce( sanitize_text_field( wp_unslash( $_POST[ $this->action_name ] ) ), $this->action )
		) {
			$data = (array) wp_unslash( $_POST['cmei'] );
			if ( $this->update( $data ) ) {
				$url = admin_url( 'options-general.php' );
				$url = add_query_arg( array( 'page' => 'cmei', 'updated' => 1 ), $url );
				wp_safe_redirect( $url );
				exit;
			} else {
				wp_die( esc_html__( 'Something went wrong.', 'correctme-instant' ) );
			}
		}

		if ( 1 === (int) get_option( 'cmei-invalid-key', 0 ) ) {
			add_action( 'admin_notices', array( $this, 'render_invalid_key_notice' ) );
		}
	}

	/**
	 * Update the settings..
	 *
	 * @param array $data The data array.
	 *
	 * @return bool
	 */
	public function update( $data ) {

		$sanitized = array();
		foreach ( $this->defaults as $key => $void ) {
			if ( ! isset( $data[ $key ] ) ) {
				return FALSE;
			}

			$sanitized[ $key ] = sanitize_text_field( $data[ $key ] );
		}

		update_option( 'cmei', $sanitized );
		update_option( 'cmei-invalid-key', 0 );

		return TRUE;
	}

	/**
	 * Add the settings page.
	 */
	public function admin_menu() {

		add_submenu_page( 'options-general.php', __( 'CorrectMe Instant', 'correct-instant' ), __( 'CorrectMe Instant', 'correct-instant' ), 'manage_options', 'cmei', array( $this, 'render' ) );
	}

	/**
	 * Register the admin scripts.
	 */
	public function register_scripts() {

		/*$min = '.min';
		if ( defined( 'WP_DEBUG' ) && WP_DEBUG ) {
			$min = '';
		}*/
		//wp_register_script( 'wcav', $this->plugin_url . 'assets/js/script' . $min . '.js', array( 'jquery' ) );
		wp_register_script( 'cmei', $this->plugin_url . 'assets/js/correctme.instant.js', array( 'jquery' ) );
	}


	/**
	 * Render the settings page.
	 */
	public function render() {

		// The form action URL.
		$url = admin_url( 'options-general.php' );
		$url = add_query_arg( array( 'page' => 'cmei' ), $url );

		// The options.
		$options = get_option( 'cmei', array() );
		$options = wp_parse_args( $options, $this->defaults );
		?>
		<div class="wrap">
			<h1><?php esc_html_e( 'CorrectMe Instant', 'correct-instant' ); ?></h1>
			<?php if ( ! empty( $_GET['key-invalid'] ) && 1 === (int) $_GET['key-invalid'] ) : // Input var okay. ?>
				<p>
					<?php esc_html_e( 'It looks like your API key is invalid or depleted. Please check your key and the account balance.', 'correct-instant' ); ?>
					<?php echo wp_kses_post( sprintf( __( 'You can log into your CorrectMe account here <a href="%s">here</a>.', 'correct-instant' ), 'https://correctme.com.ua/' ) ); ?></p>
				</p>
			<?php endif; ?>
			<form method="post" action="<?php echo esc_url( $url ); ?>">
				<?php wp_nonce_field( $this->action, $this->action_name ); ?>
				<table class="form-table">
					<tbody>
						<tr>
							<th scope="row">
								<label for="api-key"><?php esc_html_e( 'API Key', 'correct-instant' ); ?></label>
							</th>
							<td>
								<input type="text" name="cmei[api-key]" value="<?php echo esc_attr( $options['api-key'] ); ?>">
							</td>
						</tr>
					</tbody>
				</table>
				<button class="button"><?php esc_html_e( 'Update', 'correct-instant' ); ?></button>
			</form>
			<hr>
			<p>
				<?php
				esc_html_e(
					'With CorrectMe Instant you can easily verify Ukrainian postal address, correct full name and so on. Stop wasting time and money with undelivered packages or having to reach out to customers to get the correct fullname and addresses.', 'correct-instant'
				);
				?><br>
				<?php
				echo wp_kses_post(
					sprintf(
						__(
							'<a href="%1$s" target="_blank">Sign up here.</a>','correct-instant'
						),
						'https://correctme.com.ua/'
					)
				);
				?>
			</p>
		</div>
		<?php
	}

	/**
	 * Render the invalid key notice.
	 */
	public function render_invalid_key_notice() {
		?>
		<div class="notice notice-error is-dismissible">
			<p>
				<?php
				// The form action URL.
				$url = admin_url( 'options-general.php' );
				$url = add_query_arg( array( 'page' => 'cmei', 'key-invalid' => 1 ), $url );
				esc_html_e( 'Your CorrectMe API key is invalid or depleted.', 'correct-instant' );

				// Display the link to the settings page only, when we are not on the settings page.
				if ( empty( $_GET['page'] ) || 'cmei' !== sanitize_text_field( wp_unslash( $_GET['page'] ) ) ) {
					echo '<br>' . wp_kses_post(
							sprintf(
								__(
									'You can view/update the API key <a href="%s">on the settings page</a>.','correct-instant'
								),
								$url
							)
						);
				}
				?>
			</p>
		</div>
		<?php
	}
}

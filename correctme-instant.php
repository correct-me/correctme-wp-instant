<?php
/**
 * Plugin Name: CorrectMe - адреси України та ПІБ
 * Plugin URI: https://gitlab.com/correct-me/correctme-wp-instant
 * Description: Миттєва перевірка і підказка адрес України. Українські імена, прізвища, по батькові - підказка і перевірка.
 * Version: 1.0
 * Author: correctme
 * Author URI: https://correctme.com.ua/
 * License: GPL2
 * Text-Domain: correctme-instant
 *
 * @package CMEI
 **/

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

$plugin_data = get_file_data(__FILE__, array('Version' => 'Version'), false);
$plugin_version = $plugin_data['Version'];

define ( 'CMEI_PLUGIN_CURRENT_VERSION', $plugin_version );

/**
 * Load the plugin.
 */
function cmei_load() {
	require_once( dirname( __FILE__ ) . '/src/Plugin.php' );
	$plugin = new CMEI_Plugin();
	$plugin->setup();
}
add_action( 'after_setup_theme', 'cmei_load' );

add_action( 'wp_enqueue_scripts', 'cmei_plugin_enqueue' );

function cmei_plugin_enqueue() {
  wp_enqueue_script( 'jquery_1_12_4', plugins_url('/assets/js/jquery-1.12.4.js ',__FILE__), CMEI_PLUGIN_CURRENT_VERSION );
  wp_enqueue_script( 'jquery-ui-1_12_1', plugins_url('/assets/js/jquery-ui.js',__FILE__), CMEI_PLUGIN_CURRENT_VERSION );
  wp_enqueue_style( 'jquery-ui-1_12_1', '/wp-content/plugins/correctme-instant/assets/css/jquery-ui.css');

  wp_enqueue_script( 'correctme-instant', plugins_url('/assets/js/correctme.instant.js',__FILE__), CMEI_PLUGIN_CURRENT_VERSION );
  $options = get_option( 'cmei', array() );
    if ( ! empty( $options['api-key'] )  ) {
        wp_localize_script( 'correctme-instant', 'correctme_instant_token',
                array(
                    'token' => $options['api-key']
                ));
    }
}
